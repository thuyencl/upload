#!/bin/env python

import os
import glob
import sys

def _read_tab_delimited_file(image_annotation_file, has_headings=True):
    import csv

    dict_ids = []
    image_urls = []
    row_numbers = []
    cur_row_num = 0
    with open(image_annotation_file,'r') as f:
        if has_headings:
            next(f) # skip headings
            cur_row_num += 1
        reader=csv.reader(f,delimiter='\t')

        # if the file has 8 columns, use this
        # for dict_id, vocab, en_meaning, vi_meaning, image_url, notes_for_image_url, labeled_for_reuse, notes in reader:

        # if the file has 7 columns, use this
        for dict_id, vocab, en_meaning, vi_meaning, image_url, labeled_for_reuse, notes in reader:

        # if the file has 7 columns (with 2 columns for image_url), use this
        # for dict_id, vocab, vi_meaning, image_url, alternative_image_url, labeled_for_reuse, notes in reader:

        # if the file has 7 columns (with en_meaning and 2 columns for image_url), use this
        # for dict_id, vocab, en_meaning, vi_meaning, image_url, notes_for_image_url, notes in reader:

        # if the file has 6 columns, use this
        # for dict_id, vocab, vi_meaning, image_url, labeled_for_reuse, notes in reader:

        # if the file has 6 columns (with 2 columns for image_url), use this
        # for dict_id, vocab, image_url, alternative_image_url, labeled_for_reuse, notes in reader:
            dict_ids.append(dict_id)
            image_urls.append(image_url)

            cur_row_num += 1
            row_numbers.append(cur_row_num)

    return dict_ids, image_urls, row_numbers


def _check_consistency_image_annotation_file_image_dir(image_dir, image_annotation_file, has_headings=True):
    error = False

    image_dir.rstrip('/')

    dict_ids, image_urls, row_numbers = _read_tab_delimited_file(image_annotation_file, has_headings)

    # check that if url is non-empty, there should be corresponding images
    for index, url in enumerate(image_urls):
        if url and url != 'Already has image':
            cur_dict_id = dict_ids[index]
            if cur_dict_id.startswith('oxf_'):
                cur_dict_id = cur_dict_id[4:]

            #make sure there is a corresponding file
            img_path_1 = image_dir + '/' + cur_dict_id + '.jpg'
            img_path_2 = image_dir + '/' + cur_dict_id + '.JPG'
            img_path_3 = image_dir + '/' + cur_dict_id + '.png'
            img_path_4 = image_dir + '/' + cur_dict_id + '.PNG'
            if not (os.path.exists(img_path_1) or os.path.exists(img_path_2) or os.path.exists(img_path_3) or os.path.exists(img_path_4)):
                error = True
                print 'ERROR on row ' + str(row_numbers[index]) + ': Neither ' + os.path.basename(img_path_1) + ' nor ' + os.path.basename(img_path_3) + ' exists'
            
            # to re-enable for step 2, when you got back the files from the linguist
            # else:
            #     # make sure there is a corresponding 'small' file
            #     small_img_path_1 = image_dir + '/' + cur_dict_id + '_small.jpg'
            #     small_img_path_2 = image_dir + '/' + cur_dict_id + '_small.JPG'
            #     small_img_path_3 = image_dir + '/' + cur_dict_id + '_small.png'
            #     small_img_path_4 = image_dir + '/' + cur_dict_id + '_small.PNG'
            #     if not (os.path.exists(small_img_path_1) or os.path.exists(small_img_path_2) or os.path.exists(small_img_path_3) or os.path.exists(small_img_path_4)):
            #         error = True
            #         print 'ERROR of small image: Neither ' + os.path.basename(small_img_path_1) + ' nor ' + os.path.basename(small_img_path_3) + ' exists'

    # check that if there are images, then the corresponding image URL fields should be non-empty
    for img_path in glob.glob(image_dir + '/*'):
        # to re-enable for step 2, when you got back the files from the linguist

        # if '_small' in img_path:
        #     big_img_path = img_path.replace('_small', '')
        #     # if it's a 'small' image then make sure the corresponding big image exists
        #     if not os.path.exists(big_img_path):
        #         error = True
        #         print 'ERROR of big-small image pair: ' + os.path.basename(big_img_path) + ' does not exist'
        # else:

        dot_index = img_path.rfind('.')
        # small_img_path = img_path[:dot_index] + img_path[dot_index:]
        # # if it's a 'big' image then make sure the corresponding small iamge exists
        # if not os.path.exists(small_img_path):
        #     error = True
        #     print 'ERROR of big-small image pair: ' + os.path.basename(small_img_path) + ' does not exist'

        img_name = os.path.basename(img_path)
        img_name_without_extension = img_name[:img_name.rfind('.')]
        # do try-except here because if we have already checked the above, then dict_ids.index() should
        # always return something
        try:
            index = dict_ids.index(img_name_without_extension)
            if not image_urls[index]:
                error = True
                print 'ERROR on row ' + str(row_numbers[index]) + ': ' + img_name + ' exists but the URL field on this row is empty'
        except:
            pass

    if not error:
        print 'No errors!'


if __name__ == '__main__':
     image_dir = sys.argv[1]
     annotation_file = sys.argv[2]
     _check_consistency_image_annotation_file_image_dir(image_dir, annotation_file)
