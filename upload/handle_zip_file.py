# This module has 2 fuctions:
# The 1st function: to extract a zip file.
# The 2nd function: to execute script check_consystency.py

import sys
import os
import commands

#This path contains check_consistency.py file
CHECK_CONSISTENCY_PATH = '/root/mysite/upload/'

# Images path
images_path = ''

def unzip_file(zip_file_path):
    global images_path
    images_path = os.path.dirname(zip_file_path)
    # Move to folders containg images
    os.chdir(images_path)
    cmd = 'unzip -o -d . ' + zip_file_path 
    os.system(cmd)
    os.remove(zip_file_path)
    
    # If extracted file also has folder inside
    list_dir = os.listdir('.')
    for item in list_dir:
        if os.path.isdir(item):
            os.chdir(item)
            os.system('mv * ..')
           # Remove empty folder
            os.system('cd ..; rmdir ' + item)

        
def check_annotation(anno_file_path):
    os.chdir(CHECK_CONSISTENCY_PATH)
    cmd = 'python check_consistency.py ' + images_path + ' ' + anno_file_path
    (status, output) = commands.getstatusoutput(cmd)
    if status:    ## Error case, print the command's output to stderr and exit
        sys.stderr.write(output)
        sys.exit(1)
    return output

