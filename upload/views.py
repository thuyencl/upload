from datetime import date
from django.shortcuts import render
from django.http import HttpResponse
from .models import Document
import handle_zip_file

today  = date.today().strftime('%y%m%d')

def index(request):
    return render(request, 'upload/index.html')

def process_upload(request):
    if request.method == 'POST':
        tmp = request.FILES['annotation_file'].name.split('.')[0]
        #rename annoatation in format 'file_name-YYMMDD.txt'
        annotation = request.FILES['annotation_file']
        annotation.name = tmp + '-' + today + '.txt'

        instance = Document(zip=request.FILES['zip_file'], annotation=annotation)
        instance.save()

        zip_file_path = instance.zip.path
        annotation_file_path = instance.annotation.path
        handle_zip_file.unzip_file(zip_file_path)

        result = handle_zip_file.check_annotation(annotation_file_path).replace('\n', "<br/>")
        return HttpResponse(result)

